select * from banking_Info

select * from Customer_Info 

select  c.first_name ,c.last_name, b.account_type,b.branch_name 
from Banking_Info b
join Customer_Info c
on  b.account_id = c.customer_id


-- self join
select CONCAT(c.first_name ," ",c.last_name) as Full_name,ci.phone_number,c.email
from Customer_Info c
join Customer_Info ci on
c.customer_id = ci.account_id

-- left join

select c.account_id,c.first_name,c.last_name
from Customer_Info c
right join banking_Info b
on c.customer_id = b.account_id

select b.branch_name, count(c.account_id) as  Total_assoc_Branch
from banking_Info b
 cross join Customer_Info c
group by b.branch_name;

select account_type,sum(balance) as Total_Money ,count(account_id) as Total_ID, min(balance) as MIN,max(balance) as MAX
from banking_Info  
group by account_type

select b.account_id, CONCAT(c.first_name ," ",c.last_name) as Full_name,b.account_type,b.balance
from banking_Info b
left join  Customer_Info c
on b.account_id = c.account_id
where b.balance >= 500

update banking_Info
set balance = balance + (balance * 0.07) -- 7% increase 
where account_type = "Savings"

update banking_Info
set account_type = "Current"
where account_type = "Checking"

-- ? change the year to current year
-- update banking_Info
-- set last_transaction_date= YEAR(CURRENT_DATE())
-- where last_transaction_date=year(last_transaction_date)-- 

select year(last_transaction_date) from banking_Info

select customer_id, CONCAT(first_name ," ",last_name) as Full_name, TIMESTAMPDIFF(YEAR,date_of_birth,CURDATE()) AS Age 
from Customer_Info ;

select customer_id, CONCAT(first_name ," ",last_name) as Full_name, TIMESTAMPDIFF(YEAR,date_of_birth,CURDATE()) AS Age 
from Customer_Info 
having Age >=35








 








