-- Table: banking_Info
CREATE TABLE banking_Info (
    account_id INT PRIMARY KEY,
    account_type VARCHAR(50),
    balance DECIMAL(15, 2),
    branch_name VARCHAR(100),
    last_transaction_date DATE
);

-- Table: Customer_Info
CREATE TABLE Customer_Info (
    customer_id INT PRIMARY KEY,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    date_of_birth DATE,
    address VARCHAR(255),
    phone_number VARCHAR(15),
    email VARCHAR(100),
    account_id INT,  -- Foreign key referencing banking_Info
    FOREIGN KEY (account_id) REFERENCES banking_Info(account_id)
);
  show databases
  use test
    create database TEST;
    
    INSERT INTO banking_Info (account_id, account_type, balance, branch_name, last_transaction_date)
VALUES
    (1, 'Savings', 1500.00, 'Main Branch', '2023-05-15'),
    (2, 'Checking', 500.25, 'Downtown Branch', '2023-06-20'),
    (3, 'Savings', 3200.75, 'Westside Branch', '2023-06-18'),
    (4, 'Checking', 10000.00, 'Main Branch', '2023-06-19'),
    (5, 'Savings', 750.50, 'Eastside Branch', '2023-06-20'),
    (6, 'Checking', 400.00, 'Downtown Branch', '2023-06-20'),
    (7, 'Savings', 12000.00, 'Westside Branch', '2023-06-20'),
    (8, 'Checking', 800.00, 'Main Branch', '2023-06-19'),
    (9, 'Savings', 2000.00, 'Eastside Branch', '2023-06-18'),
    (10, 'Checking', 150.25, 'Downtown Branch', '2023-06-20'),
    (11, 'Savings', 500.75, 'Westside Branch', '2023-06-19'),
    (12, 'Checking', 300.00, 'Main Branch', '2023-06-18'),
    (13, 'Savings', 1800.00, 'Eastside Branch', '2023-06-20'),
    (14, 'Checking', 9000.50, 'Downtown Branch', '2023-06-20'),
    (15, 'Savings', 100.00, 'Westside Branch', '2023-06-19');
    
    
    INSERT INTO Customer_Info (customer_id, first_name, last_name, date_of_birth, address, phone_number, email, account_id)
VALUES
    (1, 'John', 'Doe', '1990-03-15', '123 Main St, Anytown, CA', '555-1234', 'john.doe@example.com', 1),
    (2, 'Jane', 'Smith', '1985-07-20', '456 Elm St, Othertown, NY', '555-5678', 'jane.smith@example.com', 2),
    (3, 'Michael', 'Johnson', '1982-05-10', '789 Oak Ave, Anycity, TX', '555-9876', 'michael.johnson@example.com', 3),
    (4, 'Emily', 'Williams', '1995-09-25', '321 Pine Rd, Someplace, FL', '555-4321', 'emily.williams@example.com', 4),
    (5, 'David', 'Brown', '1988-11-30', '654 Cedar Ln, Otherplace, WA', '555-8765', 'david.brown@example.com', 5),
    (6, 'Sarah', 'Martinez', '1992-04-12', '987 Maple Blvd, Anycity, CA', '555-2345', 'sarah.martinez@example.com', 6),
    (7, 'James', 'Garcia', '1980-08-05', '741 Birch Dr, Somewhere, AZ', '555-7890', 'james.garcia@example.com', 7),
    (8, 'Linda', 'Lopez', '1987-01-18', '852 Walnut Ave, Othercity, NV', '555-3456', 'linda.lopez@example.com', 8),
    (9, 'Robert', 'Lee', '1993-06-22', '369 Spruce St, Anytown, CA', '555-9012', 'robert.lee@example.com', 9),
    (10, 'Karen', 'Scott', '1984-12-08', '159 Pinecrest Rd, Somewhere, TX', '555-6543', 'karen.scott@example.com', 10),
    (11, 'Daniel', 'Clark', '1989-02-14', '753 Oakwood Dr, Othertown, NY', '555-2109', 'daniel.clark@example.com', 11),
    (12, 'Jennifer', 'Rodriguez', '1991-07-01', '246 Birchwood Ln, Anycity, WA', '555-5678', 'jennifer.rodriguez@example.com', 12),
    (13, 'William', 'Hernandez', '1983-04-28', '852 Cedar Ave, Someplace, FL', '555-8765', 'william.hernandez@example.com', 13),
    (14, 'Jessica', 'Young', '1986-09-13', '963 Maplewood Ct, Otherplace, CA', '555-4321', 'jessica.young@example.com', 14),
    (15, 'Christopher', 'King', '1981-03-07', '147 Oakhill Rd, Anycity, TX', '555-9876', 'christopher.king@example.com', 15);

    